package task2;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class WordsCounter {

    public static boolean printDiagram(String words) {
        if (!isValid(words)) {
            return false;
        }

        HashMap<String, Integer> map = getCountMap(words.split(" "));

        int maxValue = getMaxValue(map);
        int maxKey = getMaxKey(map);

        Map<String, String> diagram = map.entrySet().stream()
                .collect((Collectors.toMap(
                        e -> getAlignedKey(e.getKey(), maxKey),
                        e -> getDots(e.getValue(), maxValue)
                )));

        diagram.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .forEachOrdered(w -> System.out.printf("%s %s\n", w.getKey(), w.getValue()));

        return true;
    }

    private static boolean isValid(String words) {
        Pattern pattern = Pattern.compile("^[a-z ]+$");

        return pattern.matcher(words).matches();
    }

    private static HashMap<String, Integer> getCountMap(String[] words) {
        HashMap<String, Integer> map = new HashMap<>();

        for (String w : words) {
            if (map.containsKey(w)) {
                map.put(w, map.get(w) + 1);
            } else {
                map.put(w, 1);
            }
        }

        return map;
    }

    private static int getMaxValue(HashMap<String, Integer> words) {
        return words.entrySet().stream()
                .max(Map.Entry.comparingByValue(Integer::compareTo))
                .get().getValue();
    }

    private static int getMaxKey(HashMap<String, Integer> words) {
        Iterator<HashMap.Entry<String, Integer>> iterator = words.entrySet().iterator();
        
        int max = iterator.next().getKey().length();

        while (iterator.hasNext()) {
            int next = iterator.next().getKey().length();

            if (next > max) {
                max = next;
            }
        }

        return max;
    }

    private static String getDots(int current, int max) {
        int r = (current == max) ? 10 : round((double) current / max);

        StringBuilder dots = new StringBuilder();

        for (int i = 0; i < r; i++) {
            dots.append('.');
        }

        return dots.toString();
    }

    private static String getAlignedKey(String key, int len) {
        StringBuilder k = new StringBuilder(key);

        len = len == key.length() ? 0 : len - key.length();

        for (int i = 0; i < len; i++) {
            k.insert(0, '_');
        }

        return k.toString();
    }

    private static int round(double a) {
        String v = String.valueOf(a);
        char[] digits = v.substring(v.indexOf('.') + 1).toCharArray();

        int temp = 0;

        for (int i = digits.length - 1; i >= 0; i--) {
            int d = Integer.parseInt(String.valueOf(digits[i]));

            d += temp;

            if (d >= 5) {
                temp = 1;
            } else {
                temp = 0;
            }

            digits[i] = String.valueOf(d).charAt(0);
        }

        return Integer.parseInt(String.valueOf(digits[0]));
    }
}
