package task1;

class ListNode {
    private int value;
    private ListNode next;

    ListNode(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    void insert(int value) {
        if (next != null) {
            next.insert(value);
        } else {
            next = new ListNode(value);
        }
    }

    @Override
    public String toString() {
        if (next == null) return String.valueOf(value);

        return String.format("%d %s", value, next.toString());
    }
}
