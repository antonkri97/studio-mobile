package task1;

public class HashTable {
    private ListNode[] values;
    private int size;

    public HashTable(int n) {
        this.size = n;
        values = new ListNode[n];
    }

    private int hash(int x) {
        return x % size;
    }

    public void insert(int value) {
        int i = hash(value);

        if (values[i] != null) {
            values[i].insert(value);
        } else {
            values[i] = new ListNode(value);
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < values.length; i++) {
            if (values[i] == null) {
                result.append(String.format("%d: \n", i));
            } else {
                result.append(String.format("%d: %s\n", i, values[i].toString()));
            }
        }

        return result.toString();
    }
}
