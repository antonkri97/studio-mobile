package task3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimpleCalc {
    public static int evaluate(String expression) {
        if (!isValid(expression)) return 0;

        ArrayList<String> exp = new ArrayList<>(Arrays.asList(expression.split(" ")));

        while (exp.size() != 1) {
            exp = eval(exp);
        }

        return Integer.parseInt(exp.get(0));
    }

    private static boolean isValid(String expression) {
        if (!onlyDigitsAndOperators(expression)) return false;

        int digits = getDigitsNumber(expression);
        int operators = getOperatorsNumber(expression);

        return digits - operators == 1;
    }

    private static boolean onlyDigitsAndOperators(String expression) {
        Pattern pattern = Pattern.compile("^[\\d +*-\\/]+$");

        return pattern.matcher(expression).matches();
    }

    private static int getDigitsNumber(String expression) {
        Pattern pattern = Pattern.compile("\\d+");

        return getMatcherCount(pattern.matcher(expression));
    }

    private static int getOperatorsNumber(String expression) {
        Pattern pattern = Pattern.compile("[+-\\/*]+");

        return getMatcherCount(pattern.matcher(expression));
    }

    private static int getMatcherCount(Matcher matcher) {
        int count = 0;

        while (matcher.find()) {
            count++;
        }

        return count;
    }

    private static ArrayList<String> eval(ArrayList<String> exp) {
        for (int i = 0; i < exp.size(); i++) {
            if (!isOperator(exp.get(i).charAt(0))) continue;

            int f = Integer.parseInt(exp.get(i - 2));
            int s = Integer.parseInt(exp.get(i - 1));

            int r = calculate(f, s, exp.get(i).charAt(0));

            exp.set(i, String.valueOf(r));
            exp.remove(i - 2);
            exp.remove(i - 2);
            break;
        }

        return exp;
    }

    private static boolean isOperator(char ch) {
        return ch == '+' || ch == '-' || ch == '*' || ch == '/';
    }

    private static int calculate(int f, int s, char o) {
        switch (o) {
            case '+':
                return f + s;
            case '-':
                return f - s;
            case '*':
                return f * s;
            case '/':
                return f / s;
            default:
                return 0;
        }
    }
}
